--truncate table metabase.historico;
delete from metabase.historico where dt >= current_date - '10 days'::interval;
insert into metabase.historico (
 	select b.direct_prospect_id, emprestimo_id, loan_date, cohort, biz_loan, dt, 
 	dow, ldom, biz,
	biz-biz_loan as mob, 
	biz_week, biz_week_start,
	biz_month, biz_month_w,
	c_week, c_cycle, c_cycle_w,
	total_payment, total_emprestimo_original, total_pago,
	total_emprestimo_original-coalesce(total_pago,0) as saldo_emprestimo_original,
	coalesce(total_pago,0)/total_emprestimo_original as recebido_emprestimo_original,
	plano_id, portfolio_id, total_cobrado, ead, atraso_no_mes as atraso, parcelas_em_atraso, em_default_inicial, em_default, cobrado,
	pago, pagoemdia, parcelas_pagoemdia, pagosemanaatrasado, parcelas_pagosemanaatrasado, pagosemanaantecipado, parcelas_pagosemanaantecipado, pagoatrasado, parcelas_pagoatrasado, pagoantecipado, parcelas_pagoantecipado, 
	coalesce(sum(pago) over (partition by biz, emprestimo_id order by biz, emprestimo_id,dt),0) pago_no_mes,
	soft,
	coalesce(max(soft) over (partition by emprestimo_id order by dt),0) as maxatraso, 
	quitado, escritorio_cobranca_atual, 
	case when biz - biz_adm < 0 then null else biz - biz_adm end as mob_adm,
	case when biz - biz_jud < 0 then null else biz - biz_jud end as mob_jud,
	concat(to_char(dt,'yyyymmdd'),'_',emprestimo_id::text) as dt_emp,
	ltf.lead_classe_bizux, ltf.neoway_company_city, ltf.neoway_company_state, ltf.geofusion_city_region, 
	ltf.geofusion_city_segmentation,  ltf.loan_net_value, ltf.loan_is_renewal, ltf.loan_final_net_interest_rate,
	ltf.loan_original_final_pmt, ltf.loan_original_final_pmt_leverage, ltf.collection_loan_current_term, 
	ltf.loan_portfolio_id, ltf.lead_behaviour_score, c.sector, c.activity_sector, c.biz_sector, c.cnae, c.risk, c.risk_2, c.risk_3,
	ltf.lead_informed_month_revenue, 
	case when ltf.lead_informed_month_revenue <=10000 then '<010k'
	else 
		case when ltf.lead_informed_month_revenue <=20000 then '<020k'
		else 
			case when ltf.lead_informed_month_revenue <=30000 then '<030k'
			else 
				case when ltf.lead_informed_month_revenue <=40000 then '<040k'
				else 
					case when ltf.lead_informed_month_revenue <=50000 then '<050k'
					else 
						case when ltf.lead_informed_month_revenue <=80000 then '<080k'
						else 
							case when ltf.lead_informed_month_revenue <=100000 then '<100k'
							else 
								case when ltf.lead_informed_month_revenue <=150000 then '<150k'
								else 
									case when ltf.lead_informed_month_revenue <=200000 then '<200k'
									else 
										case when ltf.lead_informed_month_revenue <=400000 then '<400k'
										else 
											case when ltf.lead_informed_month_revenue <=800000 then '<800k'
											else 
												case when ltf.lead_informed_month_revenue >80000 then '>800k'
												else null 
												end 
											end 
										end 
									end 
								end 
							end 
						end 
					end 
				end 
			end 
		end 
	end as binRevenue,
	--ltf.collection_loan_amount_paid/ltf.collection_original_p_plus_i as recebido_emprestimo_original, 
	case when ltf.collection_loan_amount_paid/ltf.collection_original_p_plus_i  = 0 then '=00%'
	else
		case when ltf.collection_loan_amount_paid/ltf.collection_original_p_plus_i  <= 0.25 then '<25%'
		else
			case when ltf.collection_loan_amount_paid/ltf.collection_original_p_plus_i  <=0.50 then '<50%'
			else
				case when ltf.collection_loan_amount_paid/ltf.collection_original_p_plus_i  <=0.75 then '<75%'
				else
					case when ltf.collection_loan_amount_paid/ltf.collection_original_p_plus_i  >0.75 then '>75%'
					else null
					end
				end
			end 
		end 
	end as binPaid,
	ltf.geofusion_city_population_2017,  
	case when ltf.geofusion_city_population_2017 <=20000 then '<0m020k'
	else
		case when ltf.geofusion_city_population_2017 <=50000 then '<0m050k'
		else
			case when ltf.geofusion_city_population_2017 <=100000 then '<0m100k'
			else
				case when ltf.geofusion_city_population_2017 <=200000 then '<0m200k'
				else		
					case when ltf.geofusion_city_population_2017 <=500000 then '<0m500k'
					else		
						case when ltf.geofusion_city_population_2017 <=1000000 then '<1m000k'
						else
							case when ltf.geofusion_city_population_2017 >1000000 then '>1m000k'
							else null
							end
						end
					end
				end 
			end 
		end 
	end as binPopulation,
	ltf.lead_experian_score4 as serasa4, ltf.lead_experian_score6 as serasa6
	from (
		select 
			direct_prospect_id, o1.emprestimo_id, loan_date, to_char(loan_date,'yyyy-mm') cohort, 
			(extract(year from loan_date)-2016)*12+extract(month from loan_date)-12 biz_loan,
			o2.dt, 
			c.dow, case when c.ldom then 1 else 0 end as ldom, c.biz,
			c.biz_week, c.biz_week_start,
			c.biz_month, c.biz_month_w,
			c.c_week, c.c_cycle, c.c_cycle_w,
			total_payment, total_emprestimo_original, plano_id, portfolio_id,
			total_cobrado, 
			parcelas_em_atraso, 
			atraso_no_mes, 
			case when pago is null then em_default else lag(em_default,1) over (partition by emprestimo_id order by o2.dt) end as em_default_inicial,
			em_default, cobrado,
			pago,   
			pagoemdia, pagoSemanaAtrasado, pagoSemanaAntecipado, pagoAtrasado, pagoAntecipado,
			parcelas_pagoemdia, parcelas_pagoSemanaAtrasado, parcelas_pagoSemanaAntecipado, parcelas_pagoAtrasado, parcelas_pagoAntecipado,
			ead,
			--case when coalesce(envio_cobranca_externa, sent_date_to_collection_office) < o2.dt or sent_date_to_judicial_collection < o2.dt then coalesce(escritorio_cobranca_mongo, escritorio_cobranca) else null end escritorio_cobranca_atual ,
			case when sent_date_to_collection_office < o2.dt or sent_date_to_judicial_collection < o2.dt then escritorio_cobranca else null end escritorio_cobranca_atual ,
			-- calcula o atraso soft (somando o eventual atraso na primeira parcela da renegociação ao atraso corrente), utilizado para calcular o maior atraso de um empréstimo
			-- Se tiver mudado o empréstimo, ignora o atraso corrente e reseta.
			-- case when o2.dt = (select max(dt)- interval '39 days' from hist_aux)::date then s.soft_hist 
			--else 
			case WHEN o1.emprestimo_id <> lag(o1.emprestimo_id, 1) OVER (ORDER BY o1.emprestimo_id) THEN o2.atraso_no_mes::integer
			-- Se o empréstimo for o mesmo, verifica se o plano continua o mesmo. Se sim, só reflete o atraso no mês. Se não, soma o atraso do plano anterior ao plano atual.
	        else 
	        	case when o2.plano_id = lag(o2.plano_id, 1) OVER (ORDER BY o1.emprestimo_id) THEN 
		        	case when pago is null then o2.atraso_no_mes::integer 
		        	else 
		        		-- TBL v.01 - acrescentado a comparação: se o pago não for a parcela mais antiga, segue incrementando
			        	case when o2.atraso_no_mes::integer > lag(o2.atraso_no_mes::integer, 1) OVER (ORDER BY o1.emprestimo_id) then o2.atraso_no_mes::integer 
			        	else (lag(o2.atraso_no_mes::integer, 1) OVER (ORDER BY o1.emprestimo_id)) 
			        	end
			        end 
	            else lag(o2.atraso_no_mes::integer, 1) OVER (ORDER BY o1.emprestimo_id) 
	        	end
	        --end
			end AS soft, 
			case when ead = 0 then true else false end as quitado,
			sum(pago) over (partition by emprestimo_id order by o2.dt) as total_pago,
			biz_adm, biz_jud
		from ( 
				-- seleção de empréstimos
				 select loan_request_id as emprestimo_id, o.direct_prospect_id,loan_date, number_of_installments*approved_installment as total_emprestimo_original, portfolio_id, 
				 cr.sent_date_to_collection_office, cr.sent_date_to_judicial_collection, 
				 cr.escritorio_cobranca, li.total_payment,
				 --(jsonb_array_element(fli.data_output -> 'ExternalCollections', 0) ->>'SentDate')::date as envio_cobranca_externa,
				 --(fli.data_output -> 'CurrentExternalCollection' ->>'CollectionOffice')::text as escritorio_cobranca_mongo,
				 (extract(year from sent_date_to_collection_office)-2016)*12+extract(month from sent_date_to_collection_office)-12 as biz_adm,
				 (extract(year from sent_date_to_judicial_collection)-2016)*12+extract(month from sent_date_to_judicial_collection)-12 as biz_jud
				 from loan_Requests lr 
				 left join public.loan_infos li on lr.loan_request_id = li.loan_info_id 
				 left join public.cobranca_regua cr on lr.loan_request_id = cr.emprestimo_id 
				 left join public.offers o on o.offer_id = lr.offer_id
				 --left join public.sync_financial_loan_information fli on fli.legacy_target_id=lr.loan_request_id
				 where lr.status = 'ACCEPTED' and loan_request_id {}  --  and loan_request_id in (43,59) -- 14 -- 59 --1104 -- 1563 < 5000 --5407 -- 33024 --43 -- 59 -- 33024 -- 59 -- and loan_date between '2016-01-01' and '2020-12-31' -- where sent_date_to_collection_office is not null and emprestimo_id < 1000
			) o1
			inner join lateral 
			(
				select u.dt, 
				-- (left(dt,4)::integer-2016)*12+(substring(dt,6,2)::integer)-12 as biz,
				u.id as plano_id, v.total as total_cobrado, 
				--- Soma o total de dias em atraso (exclui: vencimento igual a data da anáise, que tenha sido pago antes ou até o vencimento [excluindo os pagos no prazo ou antecipados])
				--- contabiliza dias desde o menor vencimento em aberto (não pago ou pago depois do mês em questão) até a data da análise.
				coalesce(sum(pago_em - dia_util_ajustado) filter (where dia_util_ajustado =  u.dt and fp.pago_em is not null and dia_util_ajustado <= pago_em and pago_em = u.dt),
					u.dt - min(dia_util_ajustado) filter (where (pago_em is null or (pago_em > dia_util_ajustado and pago_em > u.dt)) and (dia_util_ajustado <=u.dt) ) ) as atraso_no_mes, 
				--- contabiliza o total de parcelas em atraso (vencimento até o último dia do mês corrente que não esteja pago ou está pago com com data maior do que o mês em questão.
				count(fp.id) filter(where (dia_util_ajustado <  u.dt) and (fp.pago_em is null or fp.pago_em >u.dt) /*and dia_util_ajustado < current_date*/) as parcelas_em_atraso, 
				--- soma o total de parcelas em atraso (vencimento até o último dia do mês corrente que não esteja pago ou está pago com com data maior do que o mês em questão.
				sum(cobrado) filter(where (dia_util_ajustado <  u.dt) and (fp.pago_em is null or fp.pago_em>u.dt) /*and dia_util_ajustado < current_date*/) em_default ,
				--- calcula o saldo do contrato
				v.total - coalesce(sum(cobrado) filter (where pago_em is not null and pago_em <= u.dt /*and dia_util_ajustado < current_date*/),0) as ead,
				-- v2.0: acrescentado a soma das parcelas pagas antecipadamente ao início da vigência do plano
				sum(cobrado) filter (where dia_util_ajustado =u.dt) as cobrado,
				sum(pago) filter (where fp.pago_em = u.dt or (fp.pago_em<=u.dt and isnovoplano)) as pago, 
				sum(pago) filter (where dia_util_ajustado = pago_em and dia_util_ajustado = u.dt) as pagoEmdia,
				count(pago) filter (where dia_util_ajustado = pago_em and dia_util_ajustado = u.dt) as parcelas_pagoEmdia,
				sum(pago) filter (where floor((extract(epoch from dia_util_ajustado)-extract(epoch from '2016-12-03'::date))/604800) = floor((extract(epoch from pago_em)-extract(epoch from '2016-12-03'::date))/604800) and dia_util_ajustado < pago_em and pago_em = u.dt ) as pagoSemanaAtrasado,
				count(pago) filter (where floor((extract(epoch from dia_util_ajustado)-extract(epoch from '2016-12-03'::date))/604800) = floor((extract(epoch from pago_em)-extract(epoch from '2016-12-03'::date))/604800) and dia_util_ajustado < pago_em and pago_em = u.dt ) as parcelas_pagoSemanaAtrasado,
				sum(pago) filter (where floor((extract(epoch from dia_util_ajustado)-extract(epoch from '2016-12-03'::date))/604800) = floor((extract(epoch from pago_em)-extract(epoch from '2016-12-03'::date))/604800) and dia_util_ajustado > pago_em and pago_em = u.dt or (floor((extract(epoch from fp.pago_em)-extract(epoch from '2016-12-03'::date))/604800)=floor((extract(epoch from u.dt)-extract(epoch from '2016-12-03'::date))/604800) and isnovoplano and u.dt>pago_em)) as pagoSemanaAntecipado,	
				count(pago) filter (where floor((extract(epoch from dia_util_ajustado)-extract(epoch from '2016-12-03'::date))/604800) = floor((extract(epoch from pago_em)-extract(epoch from '2016-12-03'::date))/604800) and dia_util_ajustado > pago_em and pago_em = u.dt or (floor((extract(epoch from fp.pago_em)-extract(epoch from '2016-12-03'::date))/604800)=floor((extract(epoch from u.dt)-extract(epoch from '2016-12-03'::date))/604800) and isnovoplano and u.dt>pago_em)) as parcelas_pagoSemanaAntecipado,
				sum(pago) filter (where floor((extract(epoch from dia_util_ajustado)-extract(epoch from '2016-12-03'::date))/604800) < floor((extract(epoch from pago_em)-extract(epoch from '2016-12-03'::date))/604800) and pago_em = u.dt) as pagoAtrasado,
				count(pago) filter (where floor((extract(epoch from dia_util_ajustado)-extract(epoch from '2016-12-03'::date))/604800) < floor((extract(epoch from pago_em)-extract(epoch from '2016-12-03'::date))/604800) and pago_em = u.dt) as parcelas_pagoAtrasado,
				sum(pago) filter (where floor((extract(epoch from dia_util_ajustado)-extract(epoch from '2016-12-03'::date))/604800) > floor((extract(epoch from pago_em)-extract(epoch from '2016-12-03'::date))/604800) and pago_em = u.dt or (floor((extract(epoch from fp.pago_em)-extract(epoch from '2016-12-03'::date))/604800)<floor((extract(epoch from u.dt)-extract(epoch from '2016-12-03'::date))/604800) and isnovoplano and u.dt>pago_em)) as pagoAntecipado,
				count(pago) filter (where floor((extract(epoch from dia_util_ajustado)-extract(epoch from '2016-12-03'::date))/604800) > floor((extract(epoch from pago_em)-extract(epoch from '2016-12-03'::date))/604800) and pago_em = u.dt or (floor((extract(epoch from fp.pago_em)-extract(epoch from '2016-12-03'::date))/604800)<floor((extract(epoch from u.dt)-extract(epoch from '2016-12-03'::date))/604800) and isnovoplano and u.dt>pago_em)) as parcelas_pagoAntecipado
				from (
					-- inclui a informação se está quitado ou não a partir do mês de quitação, inclui flag sempre que mudar um plano
					select dt, id/*, coalesce(dt>quitado_em,false) quitado*/, case when id <> lag(coalesce(id,0),1) over (order by dt) then true else false end as isNovoPlano 
					from (
						--- normaliza a lista de todos os meses, repetindo o id do plano ativo até encontrar um novo plano
						select dt, first_value(t.id) over (partition by grp) id 
						from (
						-- Cria uma lista de todos os dias desde o começo da tabela e um agrupamento (mudando o índice do grupo para cada novo plano de pagamento registrado)
							select distinct dt, per.id, count(case when per.id is not null then 1 end) over (order by dt) as grp 
							from pedro_malan.calendar c -- financeiro_parcela fp
							left join 
							(					
								-- Primeira parcela de cada plano de pagamento. Para múltiplos planos, traz o mês e o ID de cada um deles
								-- 1.7: acrescentado a comparação com lag,1 do id para os casos onde entre fechar e a primeira parcela houve renegociação e quitação (Ex: empréstimo 583)
								-- TBL v.01: acrescentado um select a mais para pegar casos onde plano + renegociação tem o mesmo vencimento da primeira parcela (pega o plano com ID maior) (Ex: empréstimo 10060)
								-- To-do: tratar casos (ex: 21376) onde há um plano ativo com pagamento no mesmo dia de uma renegociação que também vence no mesmo dia.
								-- TBL v.04: alterado min(vencimento_dia) para inicio_em, já que os planos estão com carências maiores.
								-- TBL v.05.01: [2020-06-05]: alterado para incluir a comparação com creation_date, importante para os planos POSTPONING
								select dia, max(id) as id from (
									select case when creation_date < least(entrance_Date, inicio_em) and fp."type" = 'POSTPONING' then creation_date::date else case when entrance_date < inicio_em then entrance_date else inicio_em end end dia,   
									-- case when max(fp.id)<lag(max(fp.id),1) over (order by min(vencimento_em)) then lag(max(fp.id),1) over (order by min(vencimento_em)) else max(fp.id) end id
									 max(fp.id) id
									from financeiro_planopagamento fp inner join financeiro_parcela f on f.plano_id = fp.id
									where emprestimo_id  =   o1.emprestimo_id  
									and fp.status <> 'Cancelado' and fp.status <> 'EmEspera' 
									group by fp.id
								) pl group by dia
								/* incluído o UNION para trazer o primeiro plano desde o fechamento do empréstimo (para pegar vencimentos no mesmo mês do fechamento e carências > 30 dias) */ 
								union
								select loan_date, min(id)
								from financeiro_planopagamento fp inner join public.loan_requests lr on emprestimo_id = loan_request_id 
								where emprestimo_id  =   o1.emprestimo_id  
								and fp.status <>'Cancelado' and fp.status <> 'EmEspera' 
								group by loan_date
							) per on  c.dt = per.dia
							where c.dt <= current_Date
							order by 1
						) t 
					) e	
				  	where id is not null
				 	order by 1
				) u 
				inner join financeiro_parcela fp on u.id = fp.plano_id
				inner join pedro_malan.calendar c on c.dt = fp.vencimento_em --and c.dt <= current_Date
				left join 
				(
					select plano_id, sum(cobrado) as total from financeiro_parcela group by plano_id 
				) v on v.plano_id = u.id 
				where u.dt <= current_Date
				group by u.dt, u.id, v.total--, quitado
			) o2 on true
			inner join pedro_malan.calendar c on c.dt = o2.dt
	/*		left join (
				select dt, emprestimo_id, soft as soft_hist from hist_aux where dt = (select max(dt)- interval '39 days' from hist_aux)::date
			)  s on o2.dt= s.dt and o1.emprestimo_id = s.emprestimo_id*/
	--	where o2.dt <= '2020-04-20'
	order by emprestimo_id, o2.dt
	) b
	where dt not in (metabase.historico)
	inner join lucas_leal.loan_tape_full ltf on emprestimo_id = ltf.loan_request_id 
	inner join pedro_malan.crisk c on ltf.neoway_economic_activity_national_registration_name = c.cnae
 );
grant select on metabase.historico to public;
