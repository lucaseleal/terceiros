select lead_id, 
	lead_experian_score4,
	lead_informed_month_revenue, 
	scr_months_dive_long_term_debt_pj_debt_over_total_months,
	lead_level_activity,
	scr_var_rel_12m_long_term_debt_pf_debt, 
	scr_curr_lim_cred_pf_over_application_value,
	scr_max_overdraft_pj_debt,
	scr_var_rel_12m_long_term_debt_pj_debt,
	lead_tax_health,
	activ_num,
	taxhealth_num,
	scr_var_rel_cred_limit_pf1,
	scr_curr_credit_limit_pf_over1,
	scr_max_overdraft_debt1,
	scr_var_rel_12m_cl_pj1,
	resultado_eq,
	probabilidade,
	83.00749986+288.5390082*ln((1-probabilidade)/probabilidade) as bizu2_score_calc
from (select lead_id, 
		lead_experian_score4,
		lead_informed_month_revenue, 
		scr_months_dive_long_term_debt_pj_debt_over_total_months,
		lead_level_activity,
		scr_var_rel_12m_long_term_debt_pf_debt, 
		scr_curr_lim_cred_pf_over_application_value,
		scr_max_overdraft_pj_debt,
		scr_var_rel_12m_long_term_debt_pj_debt,
		lead_tax_health,
		activ_num,
		taxhealth_num,
		scr_var_rel_cred_limit_pf1,
		scr_curr_credit_limit_pf_over1,
		scr_max_overdraft_debt1,
		scr_var_rel_12m_cl_pj1,
		resultado_eq,
		(exp(resultado_eq)/(1+EXP(resultado_eq))) as probabilidade
		from (select lead_id, 
		lead_experian_score4,
		lead_informed_month_revenue, 
		scr_months_dive_long_term_debt_pj_debt_over_total_months,
		lead_level_activity,
		scr_var_rel_12m_long_term_debt_pf_debt, 
		scr_curr_lim_cred_pf_over_application_value,
		scr_max_overdraft_pj_debt,
		scr_var_rel_12m_long_term_debt_pj_debt,
		lead_tax_health,
		activ_num,
		taxhealth_num,
		scr_var_rel_cred_limit_pf1,
		scr_curr_credit_limit_pf_over1,
		scr_max_overdraft_debt1,
		scr_var_rel_12m_cl_pj1,
		1.49697373238039+(-0.00379589934875*lead_experian_score4_0)+(-0.19921713265424*lead_informed_month_revenue1)+(-1.56691809653888*scr_months_dive_long_term_lg)+(-0.59182034869528*_0_0)+(0.69340694841013*_0_1)+(-0.37741575197994*_1_0)+(-0.22496120765458*_1_1)+(0.30997457577189*_2_0)+(_2_1*0.45077879332091)+(_2_2*-0.15471268438775)+(_3_0*-0.33024771979671)+(_3_1*-0.67861864538673)+(_3_2*0.23549846630343)+(_4_0*0.20216186611918)+(_4_1*-0.20078364480655)+(_4_2*-0.44207056876019)+(_5_0*-0.05324797180912)+(_5_1*-0.43970926664907)+(_5_2*0.49459370230532) as resultado_eq
		from (select lead_id, 
			lead_experian_score4,
			lead_informed_month_revenue, 
			scr_months_dive_long_term_debt_pj_debt_over_total_months,
			lead_level_activity,
			scr_var_rel_12m_long_term_debt_pf_debt, 
			scr_curr_lim_cred_pf_over_application_value,
			scr_max_overdraft_pj_debt,
			scr_var_rel_12m_long_term_debt_pj_debt,
			lead_tax_health,
			activ_num,
			taxhealth_num,
			scr_var_rel_cred_limit_pf1,
			scr_curr_credit_limit_pf_over1,
			scr_max_overdraft_debt1,
			scr_var_rel_12m_cl_pj1,
			case when lead_id is not null then 1 
				 else 1 end as cte,
			lead_experian_score4_0,
			lead_informed_month_revenue1,
			case when scr_months_dive_long_term_lg0 > 0.6539264674 then 0.6539264674 	
				 else scr_months_dive_long_term_lg0 end as scr_months_dive_long_term_lg,
			case when activ_num = 1 then 1
				 when activ_num = 3 then -1 
				 else 0 end as "_0_0",
			case when activ_num = 2 then 1
				 when activ_num = 3 then -1 
				 else 0 end as "_0_1",
			case when scr_var_rel_cred_limit_pf1 = 1 then 1
				 when scr_var_rel_cred_limit_pf1 = 3 then -1 
				 else 0 end as "_1_0",
			case when scr_var_rel_cred_limit_pf1 = 2 then 1
				 when scr_var_rel_cred_limit_pf1 = 3 then -1 
				 else 0 end as "_1_1",
			case when scr_curr_credit_limit_pf_over1 = 1 then 1
				 when scr_curr_credit_limit_pf_over1 = 4 then -1 
				 else 0 end as "_2_0",
			case when scr_curr_credit_limit_pf_over1 = 2 then 1
				 when scr_curr_credit_limit_pf_over1 = 4 then -1 
				 else 0 end as "_2_1",
			case when scr_curr_credit_limit_pf_over1 = 3 then 1
				 when scr_curr_credit_limit_pf_over1 = 4 then -1 
				 else 0 end as "_2_2",
			case when scr_max_overdraft_debt1 = 2 then 1
				 when scr_max_overdraft_debt1 = 5 then -1 
				 else 0 end as "_3_0",
			case when scr_max_overdraft_debt1 = 3 then 1
				 when scr_max_overdraft_debt1 = 5 then -1 
				 else 0 end as "_3_1",
			case when scr_max_overdraft_debt1 = 4 then 1
				 when scr_max_overdraft_debt1 = 5 then -1 
				 else 0 end as "_3_2",
			case when scr_var_rel_12m_cl_pj1 = 1 then 1
				 when scr_var_rel_12m_cl_pj1 = 4 then -1 
				 else 0 end as "_4_0",
			case when scr_var_rel_12m_cl_pj1 = 2 then 1
				 when scr_var_rel_12m_cl_pj1 = 4 then -1 
				 else 0 end as "_4_1",
			case when scr_var_rel_12m_cl_pj1 = 3 then 1
				 when scr_var_rel_12m_cl_pj1 = 4 then -1 
				 else 0 end as "_4_2",
			case when taxhealth_num = 1 then 1
				 when taxhealth_num = 5 then -1 
				 else 0 end as "_5_0",
			case when taxhealth_num = 2 then 1
				 when taxhealth_num = 5 then -1 
				 else 0 end as "_5_1",
			case when taxhealth_num = 4 then 1
				 when taxhealth_num = 5 then -1 
				 else 0 end as "_5_2"
		from 
		(select lead_id, 
			 lead_level_activity,
			case when lead_level_activity = 'High' then 1
				 when lead_level_activity = 'Medium' then 3
				 else 2 end as activ_num,
			 lead_tax_health, 
			 case when lead_tax_health = 'Blue' then 1
			 	  when lead_tax_health = 'Green' then 2
			 	  when lead_tax_health = 'Yellow' then 5
			 	  else 4 end as taxhealth_num,
			 scr_var_rel_12m_long_term_debt_pf_debt, 	 
			 case when scr_var_rel_12m_long_term_debt_pf_debt < 0 or scr_var_rel_12m_long_term_debt_pf_debt is null then 1
			 	  when scr_var_rel_12m_long_term_debt_pf_debt < 10 then 2 
			 	  else 3 end as scr_var_rel_cred_limit_pf1,
			scr_curr_lim_cred_pf_over_application_value,
			 case when scr_curr_lim_cred_pf_over_application_value = 0 then 1 
			 	  when (scr_curr_lim_cred_pf_over_application_value < 0.02 or scr_curr_lim_cred_pf_over_application_value is null) then 2
				  when scr_curr_lim_cred_pf_over_application_value < 0.36 then 3
				  else 4 end as scr_curr_credit_limit_pf_over1,
			scr_max_overdraft_pj_debt,
			case when scr_max_overdraft_pj_debt = 0 or scr_max_overdraft_pj_debt is null then 2
				 when scr_max_overdraft_pj_debt <3000 then 3 
				 when scr_max_overdraft_pj_debt <10000 then 4 
				 else 5 end as scr_max_overdraft_debt1,
			scr_var_rel_12m_long_term_debt_pj_debt,
			case when scr_var_rel_12m_long_term_debt_pj_debt <-0.1 or scr_var_rel_12m_long_term_debt_pj_debt is null then 1 
				 when scr_var_rel_12m_long_term_debt_pj_debt <=0 then 2 
				 when scr_var_rel_12m_long_term_debt_pj_debt <1.2 then 3 
				 else 4 end as scr_var_rel_12m_cl_pj1,
			lead_informed_month_revenue,
			case when lead_informed_month_revenue <= 20000 then 1 
				 when lead_informed_month_revenue <40000 then 2
				 when lead_informed_month_revenue <100000 then 3
				 else 4 end as lead_informed_month_revenue1,
			scr_months_dive_long_term_debt_pj_debt_over_total_months, 
			case when scr_months_dive_long_term_debt_pj_debt_over_total_months is null then 0 
				 else ln(scr_months_dive_long_term_debt_pj_debt_over_total_months+1) end as scr_months_dive_long_term_lg0,
			lead_experian_score4,
			case when lead_experian_score4 is null then 388
				 else lead_experian_score4 end as lead_experian_score4_0
			from lucas_leal."quali_tape_vfwd2.3.4_20190312") t1 ) t2) t3) t4 order by lead_id;