select  bd.dia_do_indicador, bd.emprestimo_id, bd.balanco_final, bd.balanco_final_pro_rata, bd.bacen, bd.pdd_biz_pro_rata, bd.dias_atraso, bd.dias_atraso_max, bd.portfolio_id, bd.renegociacoes,
        lr.loan_date,
        case when dp.bizu_score >= 850 then 'A+'
             when dp.bizu_score >= 700 then 'A-'
             when dp.bizu_score >= 650 then 'B+'
             when dp.bizu_score >= 600 then 'B-'
             when dp.bizu_score >= 500 then 'C+'
             when dp.bizu_score >= 400 then 'C-'
             when dp.bizu_score >= 300 then 'D'
             when dp.bizu_score < 300  then 'E'
        end as bizu_score_class,
        case when dp.bizu_score >= 700 then 'A'
             when dp.bizu_score >= 600 then 'B'
             when dp.bizu_score >= 400 then 'C'
             when dp.bizu_score >= 300 then 'D'
             when dp.bizu_score < 300  then 'E'
        end as bizu_score_class2,
        smb.tipo_emprestimo4_na_visao
from financeiro_balancodiario bd
join loan_requests lr on bd.emprestimo_id = lr.loan_request_id
join offers o on o.offer_id = lr.offer_id
join direct_prospects dp on dp.direct_prospect_id = o.direct_prospect_id
join (
        select distinct emprestimo_id, tipo_emprestimo4_na_visao                            -- ATUALIZAR
        from sandbox.saldos_mobs_ead2_bizu12_v2_fev19
      ) as smb on smb.emprestimo_id = bd.emprestimo_id
and dia_do_indicador in ('2016-12-31', '2017-01-31', '2017-02-28', '2017-03-31', '2017-04-30', '2017-05-31', '2017-06-30', '2017-07-31', '2017-08-31', '2017-09-30', '2017-10-31', '2017-11-30', '2017-12-31', '2018-01-31', '2018-02-28', '2018-03-31', '2018-04-30',
'2018-05-31', '2018-06-30', '2018-07-31', '2018-08-31','2018-09-30', '2018-10-31', '2018-11-30', '2018-12-31', '2019-01-31', '2019-02-28')            -- ATUALIZAR
order by dia_do_indicador, emprestimo_id