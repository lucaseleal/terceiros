select
	replace(text(dp.trackers -> 'gclid'), '#/', '') as "Google Click ID",
	'Leads_Value' as "Conversion Name",
	concat(to_char(dp.opt_in_date, 'YYYY-MM-DD HH24:MI:SS'), '-0300')  as "Conversion Time",
--	dp.bizu_score_class,
--	dp.amount_requested,
--	case
--		when ltv.lead_value is null then 0
--		else ltv.lead_value
	(case
		when dp.amount_requested * ltv.loan_loss * ltv.tac_perc * ltv.p_loan is null then 0
		else dp.amount_requested * ltv.loan_loss * ltv.tac_perc * ltv.p_loan
	end)::decimal(10,2)  as "Conversion Value"
from
(
	select
		*,
		case
			when bizu2_score >= 1100 then 'A+'
			when bizu2_score >= 850 then 'A-'
			when bizu2_score >= 720 then 'B+'
			when bizu2_score >= 600 then 'B-'
			when bizu2_score >= 500 then 'C+'
			when bizu2_score >= 400 then 'C-'
			when bizu2_score >= 300 then 'D'
			when bizu2_score < 300  then 'E'
			when bizu2_score is null then 'no_bizu'
		end as bizu_score_class
	from direct_prospects
)dp
join (
select
	--	dp.utm_source,
	--	count (*) as n_leads,
	--	count(*) filter (where o.status not in ('Substituida','Cancelada', 'EmEspera') and o.date_offered is not null) as n_offers,
	--	count (*) filter (where lr.status = 'ACCEPTED') as n_loans,
	--	count (*) filter (where dp.opt_in_date > now() - interval '90 days')as n_leads_90d,
	--	count(*) filter (where o.status not in ('Substituida','Cancelada', 'EmEspera') and o.date_offered is not null and o.date_offered > now() - interval '90 days') as n_offers_90d,
	--	count (*) filter (where lr.status = 'ACCEPTED' and lr.loan_date > now() - interval '90 days') as n_loans_90d
		dp.bizu_score_class,
		avg (o.max_value) filter (where dp.opt_in_date > '2019-03-10' and o.status not in ('Substituida', 'EmEspera', 'Cancelada'))::decimal(10,2) as avg_offer,
		avg (lr.value) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')::decimal(10,2) as avg_loan,
		(count (*) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')::float / count (*) filter (where dp.opt_in_date > '2019-03-10'))::decimal(10,2) as p_loan,
--		((count (*) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')::float / count (*) filter (where dp.opt_in_date > '2019-03-10'))::decimal(10,2)  * avg (li.commission) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED'))::decimal(10,2) as lead_value,
		sum(lr.value) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')::decimal(10,2) / sum (dp.amount_requested) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')  as loan_loss,
		sum(li.commission) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')::decimal(10,2) / sum (lr.value) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')  as tac_perc,
		sum(li.commission) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')::decimal(10,2) / sum (dp.amount_requested) filter (where dp.opt_in_date > '2019-03-10' and lr.status = 'ACCEPTED')  as commission_lead
	--	avg (o.max_value) filter (where o.status not in ('Substituida', 'EmEspera', 'Cancelada'))::decimal(10,2) as avg_offer,
	--	avg (lr.value) filter (where lr.status = 'ACCEPTED')::decimal(10,2) as avg_loan,
	--	(count (*) filter (where lr.status = 'ACCEPTED')::float / count (*) * 100 )::decimal(10,2) as p_loan,
	--	((count (*) filter (where lr.status = 'ACCEPTED')::float / count (*))::decimal(10,2) * avg (lr.value) filter (where lr.status = 'ACCEPTED'))::decimal(10,2) as lead_value
	from (
		select direct_prospect_id,
		name,
		name_of_responsible,
		phone,
		email,
		bizu_score,
		bizu2_score,
		bizu_score_class,
		opt_in_date,
		amount_requested,
		utm_source,
		workflow,
		case
			when utm_source in ('REALCRED','AZUL','worldsense','rentcon','','MARCOZERO','cn','payu','valmari','administradores','88CAPITAL','ALMIRGUEDES','ACESSE','mobdiq','IDEA','lio','DIGNESS','STEIN','GENESISCRED','DUOCAPITAL','R2A','CPEMPRESTIMOS','AMBEV','vidanova','CREDPRIME','SULLYVAN', 'americanas','ifood', 'finpass', 'jurosbaixos', 'finanzero', 'capitalemp', 'CREDSHOPPING', 'rsassessoria', 'MRS', 'bidu', 'andreozzi', 'vipac', 'VIRGINIA', 'IMPERIO', 'ISF', 'JEITONOVO', 'EBERT', 'CHARLES', 'CENTRALCRED', 'CREDEXPRESS', 'marketup', 'letsperform', 'ARAMAYO') then 'affiliates'
			when utm_source in ('finpass', 'peixeurbano', 'jurosbaixos', 'finanzero','prontocomb','mercado livre','partner','madeira','credhub') then 'large_partners'
	 		when utm_source in ('adwords','bing', 'android') then 'search_engine'
			when utm_source in ('twitter','facebook', 'linkedin', 'instagram') then 'social_media'
			when utm_source in ('taboola', 'vendedoor', 'trustpilot', 'NULL') then 'other_performance'
			when utm_source in ('organico', 'blog', 'rd', 'email') then 'organico'
			when utm_source in ('credita', 'experian') then 'credita'
			else 'affiliates'
		end as source,
		utm_campaign
			from
			(
				select
					name,
					name_of_responsible,
					direct_prospect_id,
					phone,
					email,
					bizu_score,
					bizu2_score,
					opt_in_date,
					amount_requested,
					workflow,
	--				case
	--					when bizu_score >= 900 then 'A+'
	--					when bizu_score >= 800 then 'A-'
	--					when bizu_score >= 700 then 'B+'
	--					when bizu_score >= 600 then 'B-'
	--					when bizu_score >= 500 then 'C+'
	--					when bizu_score >= 400 then 'C-'
	--					when bizu_score >= 300 then 'D'
	--					when bizu_score < 300  then 'E'
	--					when bizu_score is null then 'no_bizu'
	--				end as bizu_score_class,
					case
						when bizu2_score >= 1100 then 'A+'
						when bizu2_score >= 850 then 'A-'
						when bizu2_score >= 720 then 'B+'
						when bizu2_score >= 600 then 'B-'
						when bizu2_score >= 500 then 'C+'
						when bizu2_score >= 400 then 'C-'
						when bizu2_score >= 300 then 'D'
						when bizu2_score < 300  then 'E'
						when bizu2_score is null then 'no_bizu'
					end as bizu_score_class,
					coalesce(case when utm_source = 'Bing' then 'bing' when left(utm_source,4) = 'blog' then 'blog' when utm_source = 'website' or utm_source = 'direct_channel' or utm_source = 'site' then 'organico' when utm_source like '%RD%' then 'rd' when utm_source like '%andreozziz%' then 'andreozzi' else replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(replace(utm_source,'#formulario',''),'+',' '),'%28','('),'%29',')'),'%C3%BA','ÃƒÆ’Ã‚Âº'),'%C3%A7','ÃƒÆ’Ã‚Â§'),'%2B','+'),'%2F','/'),'%C3%A9','ÃƒÆ’Ã‚Â©'),'%C3%A3','ÃƒÆ’Ã‚Â£'),'#/',''),'%20',' '),'%3E','>'),'#howItWorksAnchor',''),'%23/',''),'-',''),'agilizarprocesso','') end,'NULL')
					as utm_source,
					utm_campaign
				from direct_prospects
			) dp_aux
	) as dp
	left join (
		select
			dp.direct_prospect_id,
			max(accepted_offers.offer_id) acc_offer_id,
			max(max_request_offers.offer_id) max_offer_id,
			max(no_request_offer.offer_id) other_offer_id
		from direct_prospects dp
		left join(
				select
					o.direct_prospect_id,
					o.offer_id
				from offers o
				join loan_requests lr on
					lr.offer_id = o.offer_id
				where lr.status = 'ACCEPTED'
		) as accepted_offers on
			accepted_offers.direct_prospect_id = dp.direct_prospect_id
		left join(
			select
				o.direct_prospect_id,
				max(o.offer_id) as offer_id
			from offers o
			join loan_requests lr on
			lr.offer_id = o.offer_id
			group by 1
		) as max_request_offers on
			max_request_offers.direct_prospect_id = dp.direct_prospect_id
		left join (
			select
				o.direct_prospect_id,
				max(o.offer_id) as offer_id
			from offers o
			left join loan_requests lr on
				lr.offer_id = o.offer_id
			where lr.loan_request_id is null
			group by 1
		) as no_request_offer on
			no_request_offer.direct_prospect_id = dp.direct_prospect_id
		group by 1
	) as ref_offer on
		dp.direct_prospect_id = ref_offer.direct_prospect_id
	left join offers o on o.offer_id = case when acc_offer_id is not null then acc_offer_id when max_offer_id is not null then max_offer_id else other_offer_id end
	left join (
		select lr.*
		from loan_requests lr
		join (
			select
				offer_id,
				max(loan_request_id)
				filter (where status = 'ACCEPTED' or status <> 'Substituido') as max_id,
				min(loan_request_id) as min_id
			from loan_requests
			group by 1
		) as aux on
			coalesce(max_id,min_id) = lr.loan_request_id
		) lr on
			lr.offer_id = coalesce(acc_offer_id,max_offer_id,other_offer_id)
	left join (
		select
			lr.loan_request_id,
			sum(value) over (partition by utm_source,
			to_char(loan_date, 'YYYY-MM')) as total_value
		from
			loan_requests lr
		join offers o on
			lr.offer_id = o.offer_id
		join direct_prospects dp on
			o.direct_prospect_id = dp.direct_prospect_id

		where
			lr.status = 'ACCEPTED'
		) as originacao on
		originacao.loan_request_id = lr.loan_request_id
	left join loan_infos li on li.loan_info_id = lr.loan_request_id
	group by 1
) ltv on
	ltv.bizu_score_class = dp.bizu_score_class
where replace(text(dp.trackers -> 'gclid'), '#/', '') is not null
and dp.opt_in_date > '2019-01-31'
order by 1